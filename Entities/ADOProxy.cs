﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace DACore.Entities
{
    public class ADOProxy
    {
        private DbCommand dbCommand;
        private DbConnection dbConnection;
        private DbTransaction dbTransaction;
        private DbDataReader dbDataReader;

        
        internal virtual void UpdateDBCommand(string commandText,CommandType commandType)
        {
            /*
            DbCommand comd = new SqlCommand();

            comd.CommandType = commandType;
            comd.CommandText = commandText;
            comd.Connection = dbConnection; 
            dbCommand = comd;
             * */

            dbCommand.CommandType = commandType;
            dbCommand.CommandText = commandText;
  
        }

        public CommandType CommandType
        {
            set
            {
                dbCommand.CommandType = value;
            }
        }

        public string CommandText
        {
            set
            {
                dbCommand.CommandText = value;
            }
        }

        internal virtual void CreateDBCommand()
        {
            DbCommand comd = new SqlCommand();
            comd.Connection = dbConnection;
            dbCommand = comd;
        }

        internal virtual void CreateDBConnection(string connectionString)
        {
            //string connectionString = ConfigurationManager.ConnectionStrings["wndrme"].ToString();
            //if(string.IsNullOrEmpty(connectionString))
            //    connectionString = 
            dbConnection = new SqlConnection(connectionString);
        }

        internal virtual void OpenConnection()
        {
            dbConnection.Open();
        }

        internal virtual void CloseConnection()
        {
            dbConnection.Close();
        }

        internal void BeginTransaction()
        {
            dbCommand.Transaction = dbTransaction = dbConnection.BeginTransaction();
        }

        internal void CommitTransaction()
        {
            dbTransaction.Commit();
        }

        internal void RollbackTransaction()
        {
            //* NOTE: IF TRANSACTION == NULL, IT MEANS PROBABLY THAT THE CONNECTION WAS CLOSED ALREADY.
            //* IN THIS CASE THE TRANSACTION IS AUTOMATICALLY ROLLED BACK: http://msdn.microsoft.com/en-us/library/2k2hy99x.aspx
            if (dbCommand.Transaction != null)
                dbTransaction.Rollback();
        }
        public object ExecuteScalar()
        {
            return dbCommand.ExecuteScalar();
        }

        public void ExecuteReader()
        {
            dbDataReader = dbCommand.ExecuteReader();
        }

        public void DisposeReader()
        {
            dbDataReader.Close();
        }

        public void ExecuteNonQuery()
        {
            dbCommand.ExecuteNonQuery();
        }

        public void AddParameters(string parameterName, object value, bool dbNullIfNotExist = false)
        {
            if (dbNullIfNotExist && value == null)
                value = DBNull.Value;
            else if (value == null)
                value = "";

            DbParameterCollection dbCollection = dbCommand.Parameters;
            SqlParameter sqlParam = new SqlParameter(parameterName, value);

            if (!dbCollection.Contains(sqlParam.ParameterName))
            {
                dbCollection.Add(sqlParam);
            }
            else
            {
                dbCollection[sqlParam.ParameterName] = sqlParam;
            }
        }

        public object GetParameter(string parameterName)
        {
            return dbCommand.Parameters[parameterName];
        }

        public bool HasRows()
        {
           return dbDataReader.HasRows;
        }

        public bool Read()
        {
            return dbDataReader.Read();
        }

        public bool NextResult()
        {
            return dbDataReader.NextResult();
        }

        public object GetReaderParam(string paramName)
        {
            return dbDataReader[paramName];
        }

        public T GetReaderParam<T>(string paramName) //where T:class
        {
            T result = default(T);
            object dataReader = GetReaderParam(paramName);
            if (dataReader != DBNull.Value)
                result = (T)dataReader;
            return result;
        }
    }
}
