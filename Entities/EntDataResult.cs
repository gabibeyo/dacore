﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DACore.Entities
{
    public class DataResult<T>
    {
        public List<T> Entities { get; set; }
    }
}
