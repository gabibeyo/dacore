﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DACore.Entities
{
    public class EntDataInputOutput
    {
        public Dictionary<Enum,int> IntIList { get; set; }
        public Dictionary<Enum, char> CharList { get; set; }
        public Dictionary<Enum, bool> BoolList { get; set; }
        public Dictionary<Enum, string> StringList { get; set; }
        public Dictionary<Enum, DateTime> DateTimeList { get; set; }
        public Dictionary<Enum, object> ObjectList { get; set; }
    }
}
