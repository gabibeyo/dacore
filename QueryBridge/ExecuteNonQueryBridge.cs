﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using DACore.Executors;
using DACore.Entities;

namespace DACore.QueryBridge
{
    public class ExecuteNonQueryBridge : IQueryBridgeBase
    {
        
         /// <summary>
        /// Prevent initializing object via default constroctor
        /// </summary>
        private ExecuteNonQueryBridge(ConnectionData conn) : base(conn) { }

        /// <summary>
        /// Official constructor
        /// </summary>
        /// <param name="executorBase">Concrete executor to be managed</param>
        public ExecuteNonQueryBridge(IExecutorInserter concreteQuery, ConnectionData conn)
            : base(conn)
        {
            concreteQueryObject = concreteQuery;
        }

        /// <summary>
        /// Pointer to the concrete executor that should be managed
        /// </summary>
        private IExecutorInserter concreteQueryObject;

        /// <summary>
        /// Used in transaction scenarios where same ADOProxy should be used for all queries 
        /// </summary>
        /// <param name="proxy"></param>
        public void ExecuteFacade(ADOProxy proxy)
        {
            try
            {
                proxy.UpdateDBCommand(concreteQueryObject.CommandText, concreteQueryObject.CommandType);
                concreteQueryObject.ADOProxy = proxy;
                concreteQueryObject.AddParameters();
                concreteQueryObject.Execute();
            }
            catch(Exception exe)
            {
                proxy.CloseConnection();
                throw exe;
            }
        }

        public override void ExecuteFacade()
        {
            ADOProxy proxy = new ADOProxy();
            try
            {
                proxy.CreateDBConnection(connection.String);
                proxy.OpenConnection();
                proxy.CreateDBCommand();  
                ExecuteFacade(proxy);
            }
            finally
            {
                proxy.CloseConnection();
            }
          
        }
    }
}
