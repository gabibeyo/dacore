﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using DACore.Executors;
using DACore.Entities;

namespace DACore.QueryBridge
{
    /// <summary>
    /// Specialized bridge used in Transactions scenarios
    /// </summary>
    public class ExecuteTransactionBridge : IQueryBridgeBase
    {
        
        /// <summary>
        /// Prevent initializing object via default constroctor
        /// </summary>
        private ExecuteTransactionBridge(ConnectionData conn):base(conn) { }

        /// <summary>
        /// Pointer to the concrete executor that should be managed
        /// </summary>
        private IExecutorBase concreteQueryObjects;

        /// <summary>
        /// Official constructor
        /// </summary>
        /// <param name="executorBase">Concrete executor to be managed</param>
        public ExecuteTransactionBridge(IExecutorBase concreteQuery,ConnectionData conn):base(conn)
        {
            concreteQueryObjects = concreteQuery;
        }

        public override void ExecuteFacade()
        {
            
            ADOProxy adoProxy = new ADOProxy();
            try
            {
                adoProxy.CreateDBConnection(connection.String);

                adoProxy.OpenConnection();

                //* Create DBCommand based on current IExecutorBase implementation
                adoProxy.CreateDBCommand();
                adoProxy.BeginTransaction();


                //* Pass ADOProxy to current IExecutorBase implementation, for further processing
                concreteQueryObjects.ADOProxy = adoProxy;

                //* Executr transaction
                concreteQueryObjects.Execute();

                adoProxy.CommitTransaction();
            }
            catch
            {
                adoProxy.RollbackTransaction();
                throw;
            }
            finally
            {
                adoProxy.CloseConnection();
            }
        }
    }
}
