﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DACore.Entities;

namespace DACore.QueryBridge
{
    /// <summary>
    /// Base bridge class,should be inherited by all bridges class.
    /// <remarks>
    /// QueryBridge classes are used in order to seperate the abstract behavior with the concrete implementation.
    /// QueryBridge classes are intended to manage executors
    /// </remarks>
    /// </summary>
    public abstract class IQueryBridgeBase
    {
        protected ConnectionData connection;
        public IQueryBridgeBase(ConnectionData connection)
        {
            this.connection = connection;
        }

        /// <summary>
        /// Method that manage executors
        /// </summary>
        public abstract void ExecuteFacade();
    }
}
