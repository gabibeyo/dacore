﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using DACore.Executors;
using DACore.Entities;

namespace DACore.QueryBridge
{
    /// <summary>
    /// Specialized bridge used in ExecuteReader scenarios
    /// </summary>
    /// <typeparam name="T">Any object that inherits from IExecutorBase</typeparam>
    public class ExecuteReaderBridge<T> : IQueryBridgeBase
    {
        /// <summary>
        /// Prevent initializing object via default constroctor
        /// </summary>
        private ExecuteReaderBridge(ConnectionData conn):base(conn) { }

        /// <summary>
        /// Official constructor
        /// </summary>
        /// <param name="executorBase">Concrete executor to be managed</param>
        public ExecuteReaderBridge(IExecutorReader<T> executorBase,ConnectionData conn):base(conn)
        {
            concreteQueryObject = executorBase;
        }

        /// <summary>
        /// Pointer to the concrete executor that should be managed
        /// </summary>
        private IExecutorReader<T> concreteQueryObject;


        internal void ExecuteFacade(ADOProxy proxy)
        {
            try
            {
                proxy.UpdateDBCommand(concreteQueryObject.CommandText, concreteQueryObject.CommandType);
                concreteQueryObject.ADOProxy = proxy;
                concreteQueryObject.AddParameters();
                concreteQueryObject.Execute();
                concreteQueryObject.ReadData();
            }
            catch(Exception exe)
            {
                proxy.CloseConnection();
                throw exe;
            }
        }

        public override void ExecuteFacade()
        {
            ADOProxy proxy = new ADOProxy();
            try
            {
                proxy.CreateDBConnection(connection.String);
                proxy.OpenConnection();
                proxy.CreateDBCommand();
                ExecuteFacade(proxy);
            }
            finally
            {
                proxy.CloseConnection();
            }

           
            
        }
    }
}
