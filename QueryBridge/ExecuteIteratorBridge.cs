﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using DACore.Executors;
using DACore.Entities;

namespace DACore.QueryBridge
{
    public class ExecuteIteratorBridge<T> : IQueryBridgeBase
    {
        
        /// <summary>
        /// Prevent initializing object via default constroctor
        /// </summary>
        private ExecuteIteratorBridge(ConnectionData conn):base(conn) { }

        /// <summary>
        /// Official constructor
        /// </summary>
        /// <param name="executorBase">Concrete executor to be managed</param>
        public ExecuteIteratorBridge(IExecutorIterator<T> concreteQuery, ConnectionData conn)
            : base(conn)
        {
            concreteQueryObject = concreteQuery;
        }

        /// <summary>
        /// Pointer to the concrete executor that should be managed
        /// </summary>
        private IExecutorIterator<T> concreteQueryObject;

        public void ExecuteFacade(ADOProxy proxy)
        {
            try
            {
                proxy.UpdateDBCommand(concreteQueryObject.CommandText, concreteQueryObject.CommandType);
                concreteQueryObject.ADOProxy = proxy;
                concreteQueryObject.AddParameters();
                int index = 0;
                while (concreteQueryObject.IteratorCount > index)
                {
                    concreteQueryObject.ParametersIterator(index);
                    concreteQueryObject.Execute();
                    index++;
                }
            }
            catch//* Commentout when needed(Exception exe)
            {
                proxy.CloseConnection();
            }
        }

        public override void ExecuteFacade()
        {
            ADOProxy proxy = new ADOProxy();
            try
            {
                proxy.CreateDBConnection(connection.String);
                proxy.OpenConnection();
                proxy.CreateDBCommand();
                ExecuteFacade(proxy);
            }
            finally
            {
                proxy.CloseConnection();
            }
            
        }
    }
}
