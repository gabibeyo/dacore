﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DACore.Executors
{
    public abstract class IExecutorIterator<U> : IExecutorInserter
    {

        public abstract List<U> IteratorList { get; }
        public abstract void ParametersIterator(int index);
        public int IteratorCount
        {
            get
            {
                return IteratorList.Count;
            }
        }
    }
}
