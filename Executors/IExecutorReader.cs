﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DACore.Entities;

namespace DACore.Executors
{
    /// <summary>
    /// Executor specialized in ExecuteReader scenarios.
    /// Should be used for concrete executors that use ExecuteReader
    /// </summary>
    /// <typeparam name="T">Any Entity object that the concrete executor has to fill with data</typeparam>
    public abstract class IExecutorReader<T> : IExecutorInserter
    {

        public IExecutorReader()
        {


        }
       
        /// <summary>
        /// Encapsulate executor result
        /// </summary>
        public DataResult<T> Result { get; set; }

        #region Virtual
        /// <summary>
        /// Default method used to read Reader and fill entity wit data
        /// </summary>
        public virtual void ReadData()
        {
            List<T> result = new List<T>();
            while (ADOProxy.Read())
            {
                T data = ReadRowData();
                result.Add(data);
            }
            ADOProxy.DisposeReader();
           Result = new DataResult<T>() { Entities = result };
        }

        #endregion

        #region Abstract
        /// <summary>
        /// Method used to retreive data from reader incunjuction with ReadDataRow method
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected abstract T ReadRowData();
        #endregion

        public override void Execute()
        {
           ADOProxy.ExecuteReader();
        }
    }
}
