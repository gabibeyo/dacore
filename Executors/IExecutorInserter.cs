﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;

namespace DACore.Executors
{
    public abstract class IExecutorInserter : IExecutorBase
    {
        /// <summary>
        /// SP name or parametrized querie
        /// </summary>
        public abstract string CommandText { get; }

        /// <summary>
        /// System.Data.CommandType
        /// </summary>
        public virtual System.Data.CommandType CommandType { get { return System.Data.CommandType.Text; } }

        /// <summary>
        /// Attach DataInput data to SP/Query via parameters
        /// </summary>
        /// <param name="dataInput"></param>
        public abstract void AddParameters();
    }
}
