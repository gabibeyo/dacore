﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DACore.Entities;

namespace DACore.Executors
{
    /// <summary>
    /// Base call for all StoredProcedure/Parametrized Queries/Transactions.
    /// Used in transaction scenarios
    /// </summary>
    public abstract class IExecutorBase
    {
        /// <summary>
        /// Object that handles ADO.NET operations. i.e. Create connection,open connection ect'
        /// </summary>
        public virtual ADOProxy ADOProxy { get; set; }

        /// <summary>
        /// Data to supply to SP/Query
        /// </summary>
        //internal EntDataInputOutput DataInput { get; set; }

        /// <summary>
        /// Data to supplted as output by the SP/Query
        /// </summary>
        public EntDataInputOutput DataOutput { get; set; }

        /// <summary>
        /// Execute concrete executor
        /// </summary>
        public abstract void Execute();
    }
}
